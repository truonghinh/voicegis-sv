[33mcommit 2b6638ffc10a00c4742390a14dccf8fdf65d4ffc[m
Author: truonghinh <tranquangtruonghinh@gmail.com>
Date:   Tue Nov 19 13:58:21 2013 +0700

    Add some papers

[1mdiff --git "a/03.How To/~$y tr\303\254nh ph\303\241t tri\341\273\203n \341\273\251ng d\341\273\245ng.docx" "b/03.How To/~$y tr\303\254nh ph\303\241t tri\341\273\203n \341\273\251ng d\341\273\245ng.docx"[m
[1mnew file mode 100644[m
[1mindex 0000000..f9dd77f[m
[1mdiff --git a/04.Papers/2.2_Project Charter.doc b/04.Papers/2.2_Project Charter.doc[m
[1mnew file mode 100644[m
[1mindex 0000000..ee9ce86[m
[1m--- /dev/null[m
[1m+++ b/04.Papers/2.2_Project Charter.doc[m	
[36m@@ -0,0 +1,55 @@[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Project Title:   |                                                                |[m[41m[m
[32m+[m[32m|                 |                      | |                 |                      |[m[41m[m
[32m+[m[32m|Project Sponsor: |                      | |Date Prepared:   |                      |[m[41m[m
[32m+[m[32m|                 |                      | |                 |                      |[m[41m[m
[32m+[m[32m|Project Manager: |                      | |Project Customer:|                      |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Project Purpose or Justification:                                                |[m[41m[m
[32m+[m[32m|                                                                                 |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Project Description:                                                             |[m[41m[m
[32m+[m[32m|                                                                                 |[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Summary Milestones                                              |Due Date       |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[32m|                                                                |               |[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Project Objectives        |Success Criteria          |Person Approving          |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Scope:                    |                          |                          |[m[41m[m
[32m+[m[32m|                          |                          |                          |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Time:                     |                          |                          |[m[41m[m
[32m+[m[32m|                          |                          |                          |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Cost:                     |                          |                          |[m[41m[m
[32m+[m[32m|                          |                          |                          |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Quality:                  |                          |                          |[m[41m[m
[32m+[m[32m|                          |                          |                          |[m[41m[m
[32m+[m[41m[m
[32m+[m[32m|Other:                    |                          |                          |[m[41m[m
[32m+[m[32m|                          |                          |                          |[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32mApprovals:[m[41m[m
[32m+[m[32m|                                       | |                                       |[m[41m[m
[32m+[m[32m|Project Manager Signature              | |Sponsor or Originator Signature        |[m[41m[m
[32m+[m[32m|                                       | |                                       |[m[41m[m
[32m+[m[32m|Project Manager Name                   | |Sponsor or Originator Name             |[m[41m[m
[32m+[m[32m|                                       | |                                       |[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32mDate                                         Date[m[41m[m
[1mdiff --git a/04.Papers/3.1.1_User stories.docx b/04.Papers/3.1.1_User stories.docx[m
[1mnew file mode 100644[m
[1mindex 0000000..c28b117[m
[1m--- /dev/null[m
[1m+++ b/04.Papers/3.1.1_User stories.docx[m	
[36m@@ -0,0 +1,26 @@[m
[32m+[m[32mUser stories[m[41m [m
[32m+[m[32mSTT[m
[32m+[m[32mTitle[m
[32m+[m[32mDescription[m
[32m+[m[32mStatus[m
[32m+[m[32mPiority[m
[32m+[m[32mNote[m
[32m+[m[32m1[m
[32m+[m[32mĐăng nhập[m
[32m+[m[32mNgười dùng đăng nhập vào hệ thống bằng username và password. Sau khi đăng nhập, người dùng có thể vào phần mềm, load bảng đồ, bảng dữ liệu từ CSDL ở server.[m
[32m+[m[32mView: UserLoginView[m
[32m+[m[32mVM: UserLoginViewModel[m
[32m+[m[32mModel: User[m
[32m+[m[32mRepo: UserRepository[m
[32m+[m[32mCommands: LoginCommand, CloseCommand[m
[32m+[m[32mKhi mở ứng dụng, màn hình đăng nhập sẽ hiển thị, người dùng điền tên và pass. Khi click "Đăng nhập", hệ thống sẽ tiến hành lưu username và password vào file, sau đó  kiểm tra kết nối với server. Nếu kết nối thành công, hệ thống sẽ chuyển file vừa lưu đến server. Server tiến hành kiểm tra kết nối với CSDL. Nếu kết nối thành công, Server kiểm tra username và pass. Nếu username và pass OK, Server truyền file chứa thông tin chuỗi kết nối với SDE và SQL và hiện trạng đăng nhập thành công cho Client.[m
[32m+[m[32mỞ màn hình đăng nhập, có chổ cho người dùng tùy chỉnh thông tin kết nối với server. Và có nút kiểm tra kết nối.[m
[32m+[m[32mSau khi nhận được file từ Server, Client sẽ hiện thông báo kết nối với Server thành công, và đang tiến hành kết nối với CSDL[m
[32m+[m[32mD:\private_projects\bitbucket\landpricetool\icons\Webtoys-artdesigner-lv\PNG\User_black.png[m
[32m+[m
[32m+[m[32m2[m
[32m+[m[32mCập  nhật thông tin cá nhân[m
[32m+[m[32mNgười dùng có thể xem và thay đổi thông tin cá nhân gồm: password, tên, cơ quan, số điện thoại, email[m
[32m+[m[32mD:\private_projects\bitbucket\landpricetool\icons\rockettheme-web-icon-pack-1\address_book\address_book32.png[m
[32m+[m
[32m+[m
[1mdiff --git a/04.Papers/3.1.2_Tasks.docx b/04.Papers/3.1.2_Tasks.docx[m
[1mnew file mode 100644[m
[1mindex 0000000..bf41a92[m
[1m--- /dev/null[m
[1m+++ b/04.Papers/3.1.2_Tasks.docx[m
[36m@@ -0,0 +1,17 @@[m
[32m+[m[32mTên[m
[32m+[m[32mC001  -  Đăng nhập vào ứng dụng[m
[32m+[m[32mTóm tắt[m
[32m+[m[32mNgười dùng đăng nhập vào ứng dụng có thể thực hiện tính toán và load dữ liệu[m
[32m+[m[32mLý do[m
[32m+[m[32mTránh tình trạng cập nhật dữ liệu tràn lan[m
[32m+[m[32mYêu cầu[m
[32m+[m[32mCần có bảng users trong cơ sở dư liệu ở máy chủ, phần mềm ở client có khả năng kết nối với SQL Server ở máy chủ.[m
[32m+[m[32mTên[m
[32m+[m[32mC002  -  Nhận thông số kết nối vào SDE từ Server[m
[32m+[m[32mTóm tắt[m
[32m+[m[32mPhần mềm lưu thông số kết nối vào SDE từ server dưới dạng file bin (hoặc xml) trên máy client[m
[32m+[m[32mLý do[m
[32m+[m[32mDùng thông số này để kết nối với CSDL SDE mỗi khi cần xem dữ liệu hoặc load bản đồ[m
[32m+[m[32mYêu cầu[m
[32m+[m[32mNgay sau khi đăng nhập thành công, server ghi thông tin kết nối SDE vào file (hoặc gửi dưới dạng json), và gửi cho client. Server và client cần có khả năng trao đổi file[m
[32m+[m
[1mdiff --git a/04.Papers/7.1_DailyBuildRecords.xlsx b/04.Papers/7.1_DailyBuildRecords.xlsx[m
[1mnew file mode 100644[m
[1mindex 0000000..87d29dd[m
Binary files /dev/null and b/04.Papers/7.1_DailyBuildRecords.xlsx differ
[1mdiff --git a/04.Papers/7.2_TestReport.xlsx b/04.Papers/7.2_TestReport.xlsx[m
[1mnew file mode 100644[m
[1mindex 0000000..6d0c817[m
Binary files /dev/null and b/04.Papers/7.2_TestReport.xlsx differ

[33mcommit 0b54c3aa8ab079ce4a746a90a5346e637364c165[m
Author: truonghinh <tranquangtruonghinh@gmail.com>
Date:   Tue Nov 19 10:23:06 2013 +0700

    Add some docs in How To

[1mdiff --git "a/03.How To/C\303\241ch l\341\272\255p tr\303\254nh theo m\303\264 h\303\254nh MVC.docx" "b/03.How To/C\303\241ch l\341\272\255p tr\303\254nh theo m\303\264 h\303\254nh MVC.docx"[m
[1mnew file mode 100644[m
[1mindex 0000000..9e59afd[m
[1mdiff --git "a/03.How To/Quy tr\303\254nh ph\303\241t tri\341\273\203n \341\273\251ng d\341\273\245ng.docx" "b/03.How To/Quy tr\303\254nh ph\303\241t tri\341\273\203n \341\273\251ng d\341\273\245ng.docx"[m
[1mnew file mode 100644[m
[1mindex 0000000..0a0cfdd[m
[1mdiff --git a/03.How To/mvc in java.pdf b/03.How To/mvc in java.pdf[m
[1mnew file mode 100644[m
[1mindex 0000000..eca1efe[m
[1m--- /dev/null[m
[1m+++ b/03.How To/mvc in java.pdf[m	
[36m@@ -0,0 +1,305 @@[m
[32m+[m[32mModel-View-Controller (MVC)[m[41m[m
[32m+[m[32mDesign Pattern[m[41m[m
[32m+[m[32m                Computer Science and Engineering   College of Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m  Lecture 23[m[41m[m
[32m+[m[32mMotivation[m[41m[m
[32m+[m[32m                              Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Basic parts of any application:[m[41m[m
[32m+[m[32m   Data being manipulated[m[41m[m
[32m+[m[32m   A user-interface through which this[m[41m[m
[32m+[m[32m   manipulation occurs[m[41m[m
[32m+[m[32m The data is logically independent from[m[41m[m
[32m+[m[32m how it is displayed to the user[m[41m[m
[32m+[m[32m   Display should be separately[m[41m[m
[32m+[m[32m   designable/evolvable[m[41m[m
[32m+[m[32m Example: grade distribution in class[m[41m[m
[32m+[m[32m   Displayed as both pie chart and/or bar chart[m[41m[m
[32m+[m[32m Anti-example: see BigBlob[m[41m[m
[32m+[m[32m   Presentation, logic, and state all mixed[m[41m[m
[32m+[m[32m   together[m[41m[m
[32m+[m[32mModel-View-Controller Pattern[m[41m[m
[32m+[m[32m                          Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Model[m[41m[m
[32m+[m[32m   The data (ie state)[m[41m[m
[32m+[m[32m   Methods for accessing and modifying state[m[41m[m
[32m+[m[32m View[m[41m[m
[32m+[m[32m   Renders contents of model for user[m[41m[m
[32m+[m[32m   When model changes, view must be[m[41m[m
[32m+[m[32m   updated[m[41m[m
[32m+[m[32m Controller[m[41m[m
[32m+[m[32m   Translates user actions (ie interactions[m[41m[m
[32m+[m[32m   with view) into operations on the model[m[41m[m
[32m+[m[32m   Example user actions: button clicks, menu[m[41m[m
[32m+[m[32m   selections[m[41m[m
[32m+[m[32mBasic Interactions in MVC[m[41m[m
[32m+[m[32m                                    Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m         Input[m[41m[m
[32m+[m[32m                       Controller[m[41m[m
[32m+[m[32m                                           “change data”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m             “user          “change[m[41m[m
[32m+[m[32m                                                                       Model[m[41m[m
[32m+[m[32m             action”        display”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m                         View[m[41m[m
[32m+[m[41m[m
[32m+[m[32m         Output[m[41m[m
[32m+[m[32mImplementing Basic MVC in Swing[m[41m[m
[32m+[m[32m                                Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Mapping of classes to MVC parts[m[41m[m
[32m+[m[32m   View is a Swing widget (like a JFrame & JButtons)[m[41m[m
[32m+[m[32m   Controller is an ActionListener[m[41m[m
[32m+[m[32m   Model is an ordinary Java class (or database)[m[41m[m
[32m+[m[32m Alternative mapping[m[41m[m
[32m+[m[32m   View is a Swing widget and includes (inner)[m[41m[m
[32m+[m[32m   ActionListener(s) as event handlers[m[41m[m
[32m+[m[32m   Controller is an ordinary Java class with “business[m[41m[m
[32m+[m[32m   logic”, invoked by event handlers in view[m[41m[m
[32m+[m[32m   Model is an ordinary Java class (or database)[m[41m[m
[32m+[m[32m Difference: Where is the ActionListener?[m[41m[m
[32m+[m[32m   Regardless, model and view are completely[m[41m[m
[32m+[m[32m   decoupled (linked only by controller)[m[41m[m
[32m+[m[32mMechanics of Basic MVC[m[41m[m
[32m+[m[32m                                        Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Setup[m[41m[m
[32m+[m[32m   Instantiate model[m[41m[m
[32m+[m[32m   Instantiate view[m[41m[m
[32m+[m[32m      Has reference to a controller, initially null[m[41m[m
[32m+[m[32m   Instantiate controller with references to both[m[41m[m
[32m+[m[32m      Controller registers with view, so view now has a (non-[m[41m[m
[32m+[m[32m      null) reference to controller[m[41m[m
[32m+[m[32m Execution[m[41m[m
[32m+[m[32m   View recognizes event[m[41m[m
[32m+[m[32m   View calls appropriate method on controller[m[41m[m
[32m+[m[32m   Controller accesses model, possibly updating it[m[41m[m
[32m+[m[32m   If model has been changed, view is updated (via the[m[41m[m
[32m+[m[32m   controller)[m[41m[m
[32m+[m[32m Example: CalcMVC[m[41m[m
[32m+[m[32m   CalcModel, CalcView, CalcController[m[41m[m
[32m+[m[32m   Note: View includes (gratuitous) reference to model[m[41m[m
[32m+[m[32m   Note 2: The example code has a bug! Can you find it?[m[41m[m
[32m+[m[32mExtended Interactions in MVC[m[41m[m
[32m+[m[32m                                   Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m        Input[m[41m[m
[32m+[m[32m                      Controller[m[41m[m
[32m+[m[32m                                          “change data”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m            “user[m[41m[m
[32m+[m[32m                               “I have                                Model[m[41m[m
[32m+[m[32m            action”[m[41m[m
[32m+[m[32m                               changed”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m                        View                     “give me[m[41m[m
[32m+[m[32m                                                 data”[m[41m[m
[32m+[m[32m        Output[m[41m[m
[32m+[m[32mExtended Pattern[m[41m[m
[32m+[m[32m                               Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Background: Observer pattern[m[41m[m
[32m+[m[32m   One object is notified of changes in another[m[41m[m
[32m+[m[32m   In extended MVC, view is an observer of[m[41m[m
[32m+[m[32m   model[m[41m[m
[32m+[m[32m Application within MVC[m[41m[m
[32m+[m[32m   Asynchronous model updates[m[41m[m
[32m+[m[32m     Model changes independent of user actions[m[41m[m
[32m+[m[32m     Associated view must be notified of change in[m[41m[m
[32m+[m[32m     order to know that it must update[m[41m[m
[32m+[m[32m   A model may have multiple views[m[41m[m
[32m+[m[32m     But a view has one model[m[41m[m
[32m+[m[32m     All views have to be updated when model[m[41m[m
[32m+[m[32m     changes[m[41m[m
[32m+[m[32mMechanics of Extended MVC[m[41m[m
[32m+[m[32m                                   Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Setup[m[41m[m
[32m+[m[32m   Instantiate model[m[41m[m
[32m+[m[32m     Has reference to view, initially null[m[41m[m
[32m+[m[32m   Instantiate view with reference to model[m[41m[m
[32m+[m[32m     View registers with model[m[41m[m
[32m+[m[32m   Instantiate controller with references to both[m[41m[m
[32m+[m[32m     Controller registers with view[m[41m[m
[32m+[m[32m Execution[m[41m[m
[32m+[m[32m   View recognizes event[m[41m[m
[32m+[m[32m   View calls appropriate method on controller[m[41m[m
[32m+[m[32m   Controller accesses model, possibly updating it[m[41m[m
[32m+[m[32m   If model has been changed, it notifies all[m[41m[m
[32m+[m[32m   registered views[m[41m[m
[32m+[m[32m   Views then query model for the nature of the[m[41m[m
[32m+[m[32m   change, rendering new information as appropriate[m[41m[m
[32m+[m[32mProblems with Classic MVC[m[41m[m
[32m+[m[32m                              Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Controller might need to produce its own[m[41m[m
[32m+[m[32m output[m[41m[m
[32m+[m[32m   eg Popup menu[m[41m[m
[32m+[m[32m Some state is shared between controller[m[41m[m
[32m+[m[32m and view, but does not belong in model[m[41m[m
[32m+[m[32m   eg Selection (highlighted text)[m[41m[m
[32m+[m[32m Direct manipulation means that user can[m[41m[m
[32m+[m[32m interact (control) visual elements (views)[m[41m[m
[32m+[m[32m   eg Scrollbar[m[41m[m
[32m+[m[32m Overall issue: Input and output are often[m[41m[m
[32m+[m[32m intermingled in a GUI[m[41m[m
[32m+[m[32m   Result: View and controller are tightly coupled[m[41m[m
[32m+[m[32mDelegate-Model Pattern[m[41m[m
[32m+[m[32m                           Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Model[m[41m[m
[32m+[m[32m   Data, same as before[m[41m[m
[32m+[m[32m Delegate[m[41m[m
[32m+[m[32m   Responsible for both input and output[m[41m[m
[32m+[m[32m   A combination of both view and controller[m[41m[m
[32m+[m[32m Many other names[m[41m[m
[32m+[m[32m   UI-Model[m[41m[m
[32m+[m[32m   Document-View[m[41m[m
[32m+[m[32mBasic Interactions in Delegate Model[m[41m[m
[32m+[m[32m                                     Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m          Input[m[41m[m
[32m+[m[32m                        Controller[m[41m[m
[32m+[m[32m                                            “change data”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m              “user          “change[m[41m[m
[32m+[m[32m                                                                        Model[m[41m[m
[32m+[m[32m              action”        display”[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m                          View[m[41m[m
[32m+[m[41m[m
[32m+[m[32m          Output[m[41m[m
[32m+[m[32mBasic Interactions in Delegate Model[m[41m[m
[32m+[m[32m                                Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m          Input[m[41m[m
[32m+[m[32m                   Controller[m[41m[m
[32m+[m[32m                                         “change data”[m[41m[m
[32m+[m[41m[m
[32m+[m[32m                                    “I have[m[41m[m
[32m+[m[32m                                    changed”[m[41m[m
[32m+[m[32m                                                                   Model[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m                     View                     “give me[m[41m[m
[32m+[m[32m                                              data”[m[41m[m
[32m+[m[32m          Output[m[41m[m
[32m+[m[32m                   Delegate[m[41m[m
[32m+[m[32mMechanics of Delegate Model[m[41m[m
[32m+[m[32m                              Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Setup[m[41m[m
[32m+[m[32m   Instantiate model[m[41m[m
[32m+[m[32m     As with MVC, model does not know/care about UI[m[41m[m
[32m+[m[32m   Instantiate delegate with reference to model[m[41m[m
[32m+[m[32m Execution[m[41m[m
[32m+[m[32m   Delegate recognizes event and executes[m[41m[m
[32m+[m[32m   appropriate handler for the event[m[41m[m
[32m+[m[32m   Delegate accesses model, possibly updating it[m[41m[m
[32m+[m[32m   If model has been changed, UI is updated[m[41m[m
[32m+[m[32m Example: CalcV3[m[41m[m
[32m+[m[32m   CalcModel, CalcViewController[m[41m[m
[32m+[m[32m   Note: CalcModel is exactly the same as with[m[41m[m
[32m+[m[32m   CalcMVC[m[41m[m
[32m+[m[32mNotes[m[41m[m
[32m+[m[32m                           Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Litmus test: Swapping out user[m[41m[m
[32m+[m[32m interface[m[41m[m
[32m+[m[32m   Can the model be used, without[m[41m[m
[32m+[m[32m   modification, by a completely different UI?[m[41m[m
[32m+[m[32m   eg Swing vs console text interface[m[41m[m
[32m+[m[32m Model can be easily tested with JUnit[m[41m[m
[32m+[m[32m Model actions should be quick[m[41m[m
[32m+[m[32m   GUI is frozen while model executes[m[41m[m
[32m+[m[32m   Alternative: multithreading, which gets[m[41m[m
[32m+[m[32m   much more complicated[m[41m[m
[32m+[m[32mSupplemental Reading[m[41m[m
[32m+[m[32m                          Computer Science and Engineering   The Ohio State University[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[41m[m
[32m+[m[32m Sun Developer Network[m[41m[m
[32m+[m[32m   “Java SE Application Design with MVC”[m[41m[m
[32m+[m[32m   http://java.sun.com/developer/technicalAr[m